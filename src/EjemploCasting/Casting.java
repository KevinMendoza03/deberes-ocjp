package EjemploCasting;

/**
 * Created by kevme on 4/6/2017.
 */
public class Casting {

    double a;
    double b;

    public Casting(double a, double b) {
        this.a=a;
        this.b=b;
    }

    public String toString() {
        return "a=" + a +
                ", b=" + b;
    }

    public float resultado() {
        //Casting
        float c=(float)a / (float) b;
        return c;
    }
}
