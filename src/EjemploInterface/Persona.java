package EjemploInterface;

/**
 * Created by kevme on 17/5/2017.
 */
public interface Persona {
    public int nota = 14;
    public void aprueba();
    public void suspende();
}
