package EjemploInterface;

/**
 * Created by kevme on 17/5/2017.
 */
public class Estudiante implements Persona{
    public Estudiante(){

    }

    public void aprueba(){
        System.out.println("El estudiante pasa la materia");
    }

    public void suspende(){
        System.out.println("El estudiante no pasa la materia");
    }
}
