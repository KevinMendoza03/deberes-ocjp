package EjercicioPalindromo;
import java.util.Scanner;
/**
 * Created by kevme on 2/7/2017.
 */
public class Palindrom {
    public static boolean Palindromo(String pal)
    {
        for(int i = 0; i < pal.length(); i++)
        {
            if (pal.charAt(i) != pal.charAt(pal.length() - i - 1))
            {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        String Palabra;
        Scanner a = new Scanner(System.in);
        System.out.print("Escribir palabra ");
        Palabra = a.next();

        if (Palindromo(Palabra) == true)
        {
            System.out.printf("\"%s\" es un palindromo %n", Palabra);
        } else
        {
            System.out.printf("\"%s\" no es un palindromo %n", Palabra);
        }

    }
}
