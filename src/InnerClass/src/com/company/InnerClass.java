package InnerClass.src.com.company;

//Inner Class
/*
Una clase Inner es una clase que se define dentro de otra.
Es una característica de Java que permite agrupar clases lógicamente relacionadas y
controlar la 'visibilidad' de una clase. El uso de las clases embebidas no es obvio y
contienen detallas algo más complejos que escapan del ámbito de esta introducción.
Su estructura puede ser la siguiente:

ESTRUCTURA
class Externa {
. . .
class Interna {
. . .
}
}
 */

public class InnerClass
{
    int num;
    //inner class
    private class Inner {
        public void imprimir(){
            System.out.println("Aqui esta la Inner Class");
        }
    }
    //El acceso a la clase interna del método dentro
    void mostrar_Inner(){
        Inner inner = new Inner();
        inner.imprimir();
    }

    public static void main(String[] args) {
        //Instantiating the outer class
        InnerClass outer = new InnerClass();
        //Accessing the display_Inner() method.
        outer.mostrar_Inner();
    }
}


