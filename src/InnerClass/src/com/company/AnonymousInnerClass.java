package InnerClass.src.com.company;

/*
Anonymous clase interna
Una clase interna declarada sin un nombre de clase es conocida como una clase interna anónima.
En el caso de las clases internas anónimas, declaramos y instanciarlas al mismo tiempo. Generalmente se utilizan cada vez
 que necesite reemplazar el método de una clase o una interfaz. La sintaxis de una clase interna anónima es la siguiente:

AnonymousInner an_inner = new AnonymousInner(){
   public void my_method(){
   ........
   ........
   }
};

Anonymous clase interna como argumento
En general, si un método acepta un objeto de una interfaz, una clase abstracta, o una clase concreta, entonces podemos
implementar la interfaz, extender la clase abstracta, y pasar el objeto con el método. Si se trata de una clase, entonces
podemos pasar directamente al método.
Pero en los tres casos, se puede pasar una clase interna anónima para el método. A continuación se muestra la sintaxis de
pasar una clase interna anónima como un argumento de un método:

obj.my_Method(new My_Class(){
   public void Do(){
   .....
   .....
   }
});
*/

//interface
interface Mensaje{
    String Saludo();
}

public class AnonymousInnerClass {
    //Método que acepta el objeto de la interfaz Mensaje
    public void MuestraMensaje(Mensaje m){
        System.out.println(m.Saludo() +", Este es un ejemplo de anonymous inner calss como un argumento");
    }

    public static void main(String args[]){
        //Crear instancias de la clase
        AnonymousInnerClass obj = new AnonymousInnerClass();

        //Pasar una clase interna anónima como argumento
        obj.MuestraMensaje(new Mensaje(){
            public String Saludo(){
                return "Hola";
            }
        });
    }
}