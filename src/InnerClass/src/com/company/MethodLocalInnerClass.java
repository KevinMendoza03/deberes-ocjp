package InnerClass.src.com.company;

/*
Method Local Inner Class
En Java, se puede escribir una clase dentro de un método y este será un tipo local.
Al igual que las variables locales, el alcance de la clase interna está restringido dentro del método.
Una clase interna método local puede crear una instancia única en el método en el que se define la clase interna.
El programa siguiente muestra cómo utilizar un método de clase interna local.
 */
public class MethodLocalInnerClass {
    //Instancia el metodo de la clase exterior
    void Metodo(){
        int numero = 23;

        //Metodo: method-local inner class
        class MetodoInner_Demo {
            public void imprimir(){
                System.out.println("Este es el método inner class "+numero);
            }
        }//fin del inner class

        //Accediendo al inner class
        MetodoInner_Demo inner = new MetodoInner_Demo();
        inner.imprimir();
    }


    public static void main(String[] args) {
        MethodLocalInnerClass exterior = new MethodLocalInnerClass();
        exterior.Metodo();
    }

}
