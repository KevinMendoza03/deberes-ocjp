package InnerClass.src.com.company;

/*
Estática clase anidada
Una clase interna estática es una clase anidada que es un miembro estático de la clase externa. Se puede acceder sin
crear instancias de la clase externa, el uso de otros miembros estáticos. Al igual que los miembros estáticos, una
clase anidada estática no tiene acceso a las variables y métodos de la clase externa de instancia. La sintaxis de
clase anidada estática es la siguiente:

class MyOuter {
   static class Nested_Demo{
   }
}


 */

public class StaticNestedClass{
    static class ClaseEstaticaAnidada{
        public void EjemploMetodo(){
            System.out.println("Esta es mi clase anidada");
        }
    }

    public static void main(String args[]){
        StaticNestedClass.ClaseEstaticaAnidada nested = new StaticNestedClass.ClaseEstaticaAnidada();
        nested.EjemploMetodo();
    }

}