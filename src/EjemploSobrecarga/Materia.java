package EjemploSobrecarga;

/**
 * Created by kevme on 17/5/2017.
 */
public class Materia {

    public Materia(){
        System.out.println("Materia creada");
    }

    public Materia(String nombre){
        System.out.println("La materia creada es: "+nombre);
    }

    public Materia(int creditos){
        System.out.println("La materia creada tiene : "+creditos +" creditos");
    }
}
