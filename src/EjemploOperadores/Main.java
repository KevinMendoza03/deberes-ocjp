package EjemploOperadores;

public class Main {

    public static void main(String[] args) {

        //operadores aritméticos
        int suma=0;
        suma=20-5+10+4-3+8-5;
        System.out.println("El valor de suma: "+suma);
        //el resultado es 29

        suma=20-15/3*2;
        System.out.println("El valor de suma: "+suma);
        // el resultado es 10

        //operadores relacioneales
        boolean  estado=false;
        int a=5;
        int b=8;
        estado=a<b;
        System.out.println("Valor de estado: "+estado);

        //operadores lógicos
        estado=true&&true;
        System.out.println("El resultado de estado con &&: "+estado);

        estado=false||false;
        System.out.println("El resultado de estado ||: "+estado);

        estado=!false;
        System.out.println("El resultado de estado !: "+estado);

        estado=true&&true||false&&!true;
        //true&&true||false&&!false
        //true||false&&false
        //true||false
        System.out.println("El resultado de estado con varios operadores: "+estado);

        //operadores de incremento y decremento
        int c=0;
        int d=0;
        //postfijo a++ a--
        //prefijo ++a --a
        System.out.println("Incremetno C con postfijo: "+c++);
        System.out.println("Variable C actualizada: "+c);
        System.out.println("Incremetno D con prefijo: "+ ++d);

        //operadores de asignación
        //*=, /=,+=, -=, =
        // declarado antes a=5;
        a*=a; // a* a
        System.out.println("El valor de a: "+a);

        byte x=101;
        System.out.println(++x*a);
    }
}
