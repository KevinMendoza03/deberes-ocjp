package EjemploAbstract;

/**
 * Created by kevme on 17/5/2017.
 */
public abstract class Animal {

    private String nombre;

    public Animal(){
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String s) {
        this.nombre = s;
    }

    public void comer(){
        System.out.println("El "+nombre+" esta comiendo");
    }

    public abstract void dormir();
}
