package EjemploAbstract;

/**
 * Created by kevme on 17/5/2017.
 */
public class Gato extends Animal {

    public Gato(){
        setNombre("gato");
    }

    @Override
    public void dormir() {
        System.out.println("El gato esta durmiendo");
    }
}
