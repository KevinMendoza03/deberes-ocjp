package com.proyecto;

/**
 * Created by kevme on 13/6/2017.
 */

public abstract class Estado {

    public double promedio;

    abstract public double Total();


    public double getpromedio() {
        return promedio;
    }

    public void setpromedio(double promedio) {
        this.promedio = promedio;
    }

}