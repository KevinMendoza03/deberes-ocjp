package com.proyecto;

import javax.swing.*;

/**
 * Created by kevme on 13/6/2017.
 */
public class Promedio extends Estado{

    private int nota1;
    private int nota2;


    public Promedio()
    {
        this.nota1=Integer.parseInt(JOptionPane.showInputDialog("ingresar la nota del primer bimestre : "));
        this.nota2=Integer.parseInt(JOptionPane.showInputDialog("ingresar la nota del segundo bimestre "));
    }

    @Override
    public double Total() {

        double promedio=(double) nota1 + (double) nota2;
        return promedio;
    }


    public String toString()
    {
        return "\nnota 1 : "+this.nota1 +"\nnota 2 : "+this.nota2+"\nTotal: "+Total();
    }
}
