package com.proyecto;

import javax.swing.*;


import javax.swing.JOptionPane;

public class Persona
{
    public String nombre;
    public long numeroUnico;

    public Persona(String nombre, long numeroUnico)
    {
        this.nombre = nombre;
        this.numeroUnico = numeroUnico;
    }

    public Persona() {
    }

    public String getNombres() {
        return nombre;
    }

    public void setNombres(String nombres) {
        this.nombre = nombre;
    }

    public long getNumeroUnico() {
        return numeroUnico;
    }

    public void setNumeroUnico(long numeroUnico) {
        this.numeroUnico = numeroUnico;
    }

    public String toString()
    {
        return "\nNombre : "+this.nombre+"\nNumero unico : "+this.numeroUnico;
    }
}
