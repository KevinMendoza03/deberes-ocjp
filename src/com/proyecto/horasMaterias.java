package com.proyecto;

import javax.swing.*;

/**
 * Created by kevme on 13/6/2017.
 */
public class horasMaterias implements Materia {

    protected   int a;
    protected String nombreMateria;
    protected String codigoMateria;

    public horasMaterias(int a, String nombreMateria, String codigoMateria) {
        this.a = a;
        this.nombreMateria = nombreMateria;
        this.codigoMateria = codigoMateria;
    }

    public horasMaterias()
    {
        nombreMateria= JOptionPane.showInputDialog("materia : ");
        codigoMateria= JOptionPane.showInputDialog("codigo materia: ");
        a= Integer.parseInt(JOptionPane.showInputDialog("ingrese el numero de horas de la materia : "));
    }

    @Override
    public String horas(int valor) {
        a = valor;
        return "El numero de creditos es: "+a;
    }



    @Override
    public String toString() {
        return "nombre : " + nombreMateria +
                "\ncodigo : " + codigoMateria+
                "\n"+horas(a);
    }


}
