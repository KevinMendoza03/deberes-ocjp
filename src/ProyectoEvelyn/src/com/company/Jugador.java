package com.company;

/**
 * Created by Pc Xtreme on 13/06/2017.
 */
public class Jugador extends Estado {
    public double peso;
    public double talla;

    @Override
    public double CalcularIMC() {
        return this.imc=peso/(talla*talla);
    }


    public double getPeso() {
        return peso;
    }

    public void setPeso(double peso) {
        this.peso = peso;
    }

    public double getTalla() {
        return talla;
    }

    public void setTalla(double talla) {
        this.talla = talla;
    }
}
