package com.company;

import javax.swing.*;

/**
 * Created by Pc Xtreme on 13/06/2017.
 */
public class Futbol implements Deportista {
    int horas=0;

    public Futbol(int horas) {
        this.horas = horas;
    }
    public Futbol()
    {
        horas= Integer.parseInt(JOptionPane.showInputDialog("Dato Estudiante-ingrese el nombre : "));
    }

    @Override
    public String entrenarMenos(int valor) {
        horas -=valor;
        return "El deportista de futbol menoro sus horas a: "+horas+"hh";

    }
    @Override
    public String entrenarMas(int valor) {
        String cadena="";
        horas +=valor;

        if(horas>Entrenamiento_Max)
            cadena="El deportista de futbol está excediendo sus horas de entrenamiento"+"\n";
        cadena +="El deportista de futbol practica: "+horas+"al día";
        return cadena;
    }
}
