package com.company;

/**
 * Created by Evelyn Regalado
 */
public abstract class Estado {

    public double imc;

    abstract public double CalcularIMC();

    public double getImc() {
        return imc;
    }

    public void setImc(double imc) {
        this.imc = imc;
    }

}
  