package com.company;

public class Main {

    public static void main(String[] args) {
        // write your code here

        Deportista deportista=new Basquet();
        Futbol futbol=new Futbol();

        System.out.println("\n"+"Horas de Entrenamiento de los deportistas"+ "\n");

        System.out.println("Estado del deportista de basquet es: "+ "\n" +deportista.entrenarMas(10));
        System.out.println(deportista.entrenarMenos(2));
        System.out.println("Estado del deportista de futbol es:" +"\n" +futbol.entrenarMas(8));
        System.out.println(futbol.entrenarMenos(0));

        System.out.println("\n"+"Dias de Entrenamiento de los deportistas"+ "\n");
        DiasDeSemana d;

        d = DiasDeSemana.DOMINGO; //asignamos un valor


        if (d == DiasDeSemana.DOMINGO || d == DiasDeSemana.SABADO)  //comparamos valores
            System.out.println("Estamos en fin de semana no puede Entrenar");
        else
            System.out.println("Estamos en días laborables usted puede Entrenar");

        System.out.println("\n"+"Estado de salud de los deportistas"+ "\n");
        Jugador jugador = new Jugador();
        jugador.setPeso(50);
        jugador.setTalla(1.60);
        System.out.println("El IMC del jugador es: " + jugador.CalcularIMC());
        if (jugador.CalcularIMC() > 18.5 && jugador.CalcularIMC() < 24.9) {
            System.out.println("ESTAS EN UN PESO IDEAL O NORMAL" + "\n");
        }

        if (jugador.CalcularIMC() < 18.5) {
            System.out.println("ESTAS BAJO DE PESO O DESNUTRIDA" + "\n");
        }

        if (jugador.CalcularIMC() > 25 && jugador.CalcularIMC() < 29.9) {
            System.out.println("ESTAS EN SOBRE PRESO" + "\n");
        }

        if (jugador.CalcularIMC() > 30) {
            System.out.println("ESTAS OBESO" + "\n");
        }

    }
}
