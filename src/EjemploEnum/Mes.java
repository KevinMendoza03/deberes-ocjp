package EjemploEnum;

/**
 * Created by kevme on 17/5/2017.
 */
public enum Mes {
    ENERO(true),
    FEBRERO(false),
    MARZO(true),
    ABRIL(false),
    MAYO(true),
    JUNIO(false),
    JULIO(true),
    AGOSTO(false),
    SEPTIEMBRE(true),
    OCTUBRE(false),
    NOBIEMBRE(true),
    DICIEMBRE(false);

    private boolean impar;

    private Mes(boolean impar){
        this.impar = impar;
    }

    @Override
    public String toString() {

        if(impar){
            return "El mes "+ this.name().toLowerCase() +" es impar";
        }else{
            return "El mes "+  this.name().toLowerCase() +" es par";
        }

    }
}