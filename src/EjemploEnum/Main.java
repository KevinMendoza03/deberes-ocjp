package EjemploEnum;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {

        Scanner sn = new Scanner(System.in);
        System.out.println("Escribe un mes : ");
        String numero = sn.next();

        Mes meses = Mes.valueOf(numero.toUpperCase());

        System.out.println(meses.toString());

    }
}
